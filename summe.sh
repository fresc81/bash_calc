#!/bin/bash

START=$1
ENDE=$2

LAUF=$START
AUSDRUCK=
SUMME=0

if [ "$START" != "" ] && [ "$ENDE" != "" ] && [ "$LAUF" -ne "$ENDE" ]; then

  while [ "$LAUF" -le "$ENDE" ]; do

    if [ "$LAUF" -eq "$START" ]; then
      AUSDRUCK="$LAUF";
    else
      AUSDRUCK="$AUSDRUCK + $LAUF";
    fi

    SUMME=`expr $SUMME + $LAUF`;
    LAUF=`expr $LAUF + 1`;

  done

  echo "$AUSDRUCK = $SUMME"

else

  echo "$0 - Addiert alle Integerwerte in einem  Intervall.";
  echo "Benutzung:";
  echo "  $0 WERT1 WERT2";
  echo "    WERT1 und WERT2 müssen Integerwerte sein.";
  echo "    WERT1 muss kleiner als WERT2 sein.";

fi
