#!/bin/bash

# sqrt funktion
function sqrt {
  echo "sqrt($1)" | bc;
}

# würfel funktion
function random {
  local result;
  result=`expr $RANDOM % 8`;
  while [ \( $result -eq 6 \) -o \( $result -eq 7 \) ]; do
    result=`expr $RANDOM % 8`;
  done;
  echo $result;
}

# Anzahl der Würfe
echo -n "Bitte Anzahl der Würfe eingeben: "
read lauf

# 6*N=lauf
N=`expr $lauf / 6`

# sqrt(N)
sqrt_N=$(sqrt $N)

# Schranken
schranke_unten=`expr $N - $sqrt_N`
schranke_oben=`expr $N + $sqrt_N`

echo "Schranken: [ $schranke_unten ; $schranke_oben ]"

# N[i]
# wuerfe array initialisieren
i=0
while [ $i -lt 6 ]; do
  wuerfe[$i]=0;
  i=`expr $i + 1`;
done

# würfeln
while [ $lauf -gt 0 ]; do
  wurf=$(random);
  wuerfe[$wurf]=`expr ${wuerfe[$wurf]} + 1`;
  lauf=`expr $lauf - 1`;
done

# ausgabe
i=0
while [ $i -lt 6 ]; do
  wurf="${wuerfe[$i]}";
  if [ \( $schranke_unten -le $wurf \) -a \( $wurf -le $schranke_oben \) ]; then
    schranke_ok="  ja";
  else
    schranke_ok="nein";
  fi;
  echo "Anzahl der $(expr $i + 1)'er => $wurf (Schranke eingehalten: $schranke_ok)";
  i=`expr $i + 1`;
done
