# bash_calc #

  Bash Script [Übung 2](https://bitbucket.org/fresc81/bash_calc/downloads/Uebungen_2.pur.pdf)

## zu 2.1 ##

        while [...];do
        ...
        done < namen.dat
  
  Diese Konstruktion bewirkt, dass die Datei 'namen.dat' für die Dauer der
  _while_-Schleife in die Standardeingabe umgeleitet wird.
  Mit dem _read_-Befehl kann nun die Datei 'namen.dat' gelesen werden:
  
        while read line;do
          echo "Zeile: $line";
        done < namen.dat
  
## zu 2.7 ##

  Der Wertebereich von _$RANDOM_ liegt zwischen 0 und 32767. Mit dem Modulo-Operator
  kann dieser Wertebereich auf 0 bis 5 abgebildet werden, zählt man 1 dazu - erhält man
  einen Wert von 1 bis 6.
  
  Da die Ergebnisse der Operation _$RANDOM_%_6_ aber nicht gleichmäßig über den Wertebereich
  verteilt sind (32768 ist nicht ganzzahlig durch 6 teilbar) ist auch die erzeugte Zufallszahl
  nicht gleichverteilt.
  
  Man kann aber eine Zahl zwischen 0 und 7 generieren (32768 ist ganzzahlig durch 8 teilbar),
  alle 6er und 7er "wegwerfen" und solange neu generieren, bis ein Wert zwischen 0 und 5
  erzeugt wurde.
  