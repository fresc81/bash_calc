#!/bin/bash

echo '+-----------------------------+'
printf '| %16s | %8s |\n' Wort Anzahl
echo '+-----------------------------+'
cat $* | tr "[A-Z]" "[a-z]" | sed -re 's/[[:punct:]]+/ /g' | awk 'BEGIN { RS = "[[:space:]]+" } ; { print $0 }' | sort | uniq -c | sort -nr | while read anzahl wort; do
  if [ "$wort" != "" ]; then
    printf '| %-16s | %8s |\n' "$wort" "$anzahl";
  fi;
done
echo '+-----------------------------+'
